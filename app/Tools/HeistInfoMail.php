<?php

namespace App\Tools;

class HeistInfoMail 
{
    public $name;
    public $email;
    public $message;

    public function __construct($name, $email, $message){
        $this->name = $name;
        $this->email = $email;
        $this->message = $message;
    }
}
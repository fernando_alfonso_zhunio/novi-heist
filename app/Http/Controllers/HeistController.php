<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreHeistRequest;
use App\Http\Requests\UpdateHeistRequest;
use App\Mail\HeistMail;
use App\Models\Heist;
use App\Tools\HeistInfoMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

class HeistController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $galleries = Heist::paginate();
        return Inertia::render('Heists/HeistIndex', ['heists' => $galleries]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreHeistRequest $request)
    {
        if (!$request->hasFile('file')) {
            $validator = \Illuminate\Validation\ValidationException::withMessages([
                'file' => ['required', 'mimes:jpg,jpeg,png,gif,mp4,avi,mkv'],
            ]);
            throw $validator;
        }
        $file = $request->file('file');
        $extension = $file->getClientOriginalExtension();

        $typeFile = $this->getTypeFile($extension);

        $nombreArchivo = uniqid() . '.' . $extension;

        $file->storeAs('images/heist', $nombreArchivo);

        Heist::create([
            'path' => '/images/heist/' . $nombreArchivo,
            'name' => $request->name,
            'description' => $request->description,
            'reward' => $request->reward,
            'city' => $request->city,
            'title' => $request->title,
            'type' => $typeFile
        ]);
        return to_route('heist.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Heist $gallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Heist $gallery)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateHeistRequest $request, Heist $heist)
    {
        $data = $request->all();
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension();
            $typeFile = $this->getTypeFile($extension);
            $nombreArchivo = uniqid() . '.' . $extension;

            $path = substr($heist->path, 1);
            if (Storage::exists($path)) {
                Storage::delete($path);
            }

            $file->storeAs('images/heist', $nombreArchivo);
            $data['path'] = '/images/heist/' . $nombreArchivo;
            $data['type'] = $typeFile;
        }

        $heist->update($data);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Heist $heist)
    {

        $path = substr($heist->path, 1);
        if (Storage::exists($path)) {
            Storage::delete($path);
        }
        $heist->delete();
        return to_route('heist.index');
    }

    public function sendMail(Request $request) {
        $request->validate([
            'name' => 'required| string',
            'email' => 'nullable|email',
            'message' => 'required|string',
        ]);
        $heistInfoMail = new HeistInfoMail('hjhj', 'hj', 'erbabd');
        Mail::send(new HeistMail($heistInfoMail));
        return response()->json([
            'data' => $heistInfoMail,
            'status' => 200
        ]);
    }

    private function getTypeFile($extension)
    {
        $tipoArchivo = 'desconocido';
        if (in_array($extension, ['jpg', 'jpeg', 'png', 'gif'])) {
            $tipoArchivo = 'img';
        } elseif (in_array($extension, ['mp4', 'avi', 'mkv'])) {
            $tipoArchivo = 'video';
        }
        return $tipoArchivo;
    }
}

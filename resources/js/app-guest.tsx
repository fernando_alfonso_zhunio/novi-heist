import "../css/app-guest.css";
import { AlertFz } from "./tools/alert";
// import { Carousel } from "./tools/carousel-guest";

// const carousel = new Carousel("#carousel");
// carousel.init();
let heists: any[] = [];
let contentHeistElement: HTMLDivElement | null = null;
let currentIndex = 0;
const alertCustom = new AlertFz("#alert-custom");
const btnVerify = document.getElementById("btn-verify") as HTMLButtonElement;
const btnDropdownMenu = document.getElementById(
    "btn-dropdown-menu"
) as HTMLButtonElement;
const dropdownMenu = document.getElementById("dropdown-menu") as HTMLDivElement;
const skeletonHeistsElement = document.getElementById(
    "skeleton-heists"
) as HTMLDivElement;

const btnSendMail = document.getElementById(
    "btn-send-mail"
) as HTMLButtonElement;

btnVerify?.addEventListener("click", (e) => {
    e.preventDefault();
    const imei = document.getElementById("input-imei") as HTMLInputElement;
    if (!imei.value || imei.value.trim().length < 1) {
        alertCustom.show({ title: "Ingresa un imei", message: "" });
        return;
    }
    btnVerify.disabled = true;
    btnVerify.classList.add("is-loading");
    getVerifyImei(imei.value)
        .then((data) => {
            if (data.data) {
                alertCustom.show({
                    title: "Este IMEI esta reportado",
                    message: `
                ${data.data.imei} <br> ${data.data.name} <br> ${data.data.code} <br>
            <div class="mt-4 text-center w-full">
                <p>Tu Información es Confidencial: Nos tomamos la privacidad en serio. Tu identidad estará protegida cuando proporciones información sobre los culpables de esta venta. <br>
                    Contáctenos, por favor.
                </p>
            </div>
            <div class="mt-4 text-center w-full">
                <a class="center gap-2">
                    <svg xmlns="http://www.w3.org/2000/svg" height="2.5rem"  viewBox="0 0 512 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M64 112c-8.8 0-16 7.2-16 16v22.1L220.5 291.7c20.7 17 50.4 17 71.1 0L464 150.1V128c0-8.8-7.2-16-16-16H64zM48 212.2V384c0 8.8 7.2 16 16 16H448c8.8 0 16-7.2 16-16V212.2L322 328.8c-38.4 31.5-93.7 31.5-132 0L48 212.2zM0 128C0 92.7 28.7 64 64 64H448c35.3 0 64 28.7 64 64V384c0 35.3-28.7 64-64 64H64c-35.3 0-64-28.7-64-64V128z"/></svg>                   
                    denuncias@novicompu.com
                </a>
            </div>
            <div class="mt-4 text-center w-full">
                <a class="items-center inline-flex gap-2 py-2 px-4 bg-green-600 hover:bg-green-400 text-white rounded-md" href="https://api.whatsapp.com/send/?phone=%2B593982530072&text&app_absent=0" target="_blank">
                <svg fill="white" xmlns="http://www.w3.org/2000/svg" height="2rem" viewBox="0 0 448 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M380.9 97.1C339 55.1 283.2 32 223.9 32c-122.4 0-222 99.6-222 222 0 39.1 10.2 77.3 29.6 111L0 480l117.7-30.9c32.4 17.7 68.9 27 106.1 27h.1c122.3 0 224.1-99.6 224.1-222 0-59.3-25.2-115-67.1-157zm-157 341.6c-33.2 0-65.7-8.9-94-25.7l-6.7-4-69.8 18.3L72 359.2l-4.4-7c-18.5-29.4-28.2-63.3-28.2-98.2 0-101.7 82.8-184.5 184.6-184.5 49.3 0 95.6 19.2 130.4 54.1 34.8 34.9 56.2 81.2 56.1 130.5 0 101.8-84.9 184.6-186.6 184.6zm101.2-138.2c-5.5-2.8-32.8-16.2-37.9-18-5.1-1.9-8.8-2.8-12.5 2.8-3.7 5.6-14.3 18-17.6 21.8-3.2 3.7-6.5 4.2-12 1.4-32.6-16.3-54-29.1-75.5-66-5.7-9.8 5.7-9.1 16.3-30.3 1.8-3.7.9-6.9-.5-9.7-1.4-2.8-12.5-30.1-17.1-41.2-4.5-10.8-9.1-9.3-12.5-9.5-3.2-.2-6.9-.2-10.6-.2-3.7 0-9.7 1.4-14.8 6.9-5.1 5.6-19.4 19-19.4 46.3 0 27.3 19.9 53.7 22.6 57.4 2.8 3.7 39.1 59.7 94.8 83.8 35.2 15.2 49 16.5 66.6 13.9 10.7-1.6 32.8-13.4 37.4-26.4 4.6-13 4.6-24.1 3.2-26.4-1.3-2.5-5-3.9-10.5-6.6z"/></svg>
                     0982530072
                </a>
            </div>
                `,
                });
            } else {
                alertCustom.show({
                    title: "Verificación exitosa",
                    message:
                        "Tu imei no esta registrado como robado. Felicidades",
                });
            }
            btnVerify.disabled = false;
            btnVerify.classList.remove("is-loading");
        })
        .catch((error) => {
            btnVerify.disabled = false;
            alertCustom.show({
                title: "Verificación fallida",
                message: "Intentalo de nuevo",
            });
            btnVerify.classList.remove("is-loading");
        });
});

btnDropdownMenu?.addEventListener("click", (e) => {
    e.preventDefault();
    e.stopPropagation();
    dropdownMenu.classList.toggle("hidden");
});

document.addEventListener("click", (e) => {
    if (!dropdownMenu.classList.contains("hidden")) {
        dropdownMenu.classList.add("hidden");
    }
});

btnSendMail?.addEventListener("click", (e) => {
    e.preventDefault();
    e.stopPropagation();
    const names = document.getElementsByName("names")[0] as HTMLInputElement;
    const email = document.getElementsByName("email")[0] as HTMLInputElement;
    const message = document.getElementsByName(
        "message"
    )[0] as HTMLInputElement;
    
    if (!names.value || !message.value) {
        alertCustom.show({
            title: "Faltan datos por llenar",
            message: "Los datos obligatorios son los nombres y mensaje",
        });
        return;
    }
    btnSendMail.disabled = true;
    btnSendMail.classList.add("is-loading");

    fetch("/send-mail", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-Token": (document.querySelector(
                'input[name="_token"]'
            ) as any)!.value,
        },
        body: JSON.stringify({
            name: names.value,
            email: email.value,
            message: message.value,
        }),
    })
        .then((data) => {
            btnSendMail.disabled = false;
            console.log({ data });
            if (data.status === 200) {
                alertCustom.show({
                    title: "Mensaje Enviado",
                    message: "Mensaje Enviado Correctamente, Gracias!",
                });
            }
            if (data.status != 200) {
                alertCustom.show({
                    title: "Mensaje no Enviado",
                    message: "Inténtalo de nuevo",
                });
            }
            btnSendMail.classList.remove("is-loading");
        })
        .catch((error) => {
            btnSendMail.disabled = false;
            alertCustom.show({
                title: "Mensaje no Enviado",
                message: "Inténtalo de nuevo",
            });
            btnSendMail.classList.remove("is-loading");
        });
});

async function init() {
    const data = await getHeists();
    heists = data.heists.data;
    contentHeistElement = document.getElementById(
        "content-heists"
    ) as HTMLDivElement;
    for (let index = 0; index < 3; index++) {
        const element = heists[index];
        currentIndex = index;
        element && addIframeHeist(element);
    }

    setTimeout(() => {
        skeletonHeistsElement.innerHTML = "";
        currentIndex++;
        for (let index = currentIndex; index < heists.length; index++) {
            const element = heists[currentIndex];
            element && addIframeHeist(element);
            currentIndex++;
        }
    }, 6000);
}

init();

function addIframeHeist(heist: any) {
    const html = `<div class="bg-white rounded-lg">
    <iframe loading="lazy" width="100%" height="auto" controls class="h-auto aspect-square object-contain bg-black max-w-full rounded-lg" src="https://www.youtube.com/embed/${heist.path}" alt=""></iframe>   
    <div class="px-7 py-3">
        <h4 class="font-bold mb-3">${heist.name}</h4>
        <button id="btn-heist-${heist.id}" data-id="${heist.id}" class="text-white font-light inline-block text-sm bg-blue-600 p-3 rounded-md" href="http://" target="_blank" rel="noopener noreferrer">Tengo información</button>
    </div>
    </div>`;
    contentHeistElement?.insertAdjacentHTML("beforeend", html);
    const btnHeist = document.getElementById(`btn-heist-${heist.id}`);
    btnHeist?.addEventListener("click", (e: any) => {
        alertCustom.show({
            title: "Información",
            message: `<div class="flex flex-col justify-center">
            <div class="mt-4 text-center w-full">
                <p>Tu Información es Confidencial: Nos tomamos la privacidad en serio. Tu identidad estará protegida cuando proporciones información sobre los culpables. <br>
                    Contáctanos, estamos aquí para ayudarte en todo momento.
                </p>
            </div>
            <div class="mt-4 text-center w-full">
                <a class="center gap-2">
                    <svg xmlns="http://www.w3.org/2000/svg" height="2.5rem"  viewBox="0 0 512 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M64 112c-8.8 0-16 7.2-16 16v22.1L220.5 291.7c20.7 17 50.4 17 71.1 0L464 150.1V128c0-8.8-7.2-16-16-16H64zM48 212.2V384c0 8.8 7.2 16 16 16H448c8.8 0 16-7.2 16-16V212.2L322 328.8c-38.4 31.5-93.7 31.5-132 0L48 212.2zM0 128C0 92.7 28.7 64 64 64H448c35.3 0 64 28.7 64 64V384c0 35.3-28.7 64-64 64H64c-35.3 0-64-28.7-64-64V128z"/></svg>                   
                    denuncias@novicompu.com
                </a>
            </div>
            <div class="mt-4 text-center w-full">
                <a class="items-center inline-flex gap-2 py-2 px-4 bg-green-600 hover:bg-green-400 text-white rounded-md" href="https://api.whatsapp.com/send/?phone=%2B593982530072&text&app_absent=0" target="_blank">
                <svg fill="white" xmlns="http://www.w3.org/2000/svg" height="2rem" viewBox="0 0 448 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M380.9 97.1C339 55.1 283.2 32 223.9 32c-122.4 0-222 99.6-222 222 0 39.1 10.2 77.3 29.6 111L0 480l117.7-30.9c32.4 17.7 68.9 27 106.1 27h.1c122.3 0 224.1-99.6 224.1-222 0-59.3-25.2-115-67.1-157zm-157 341.6c-33.2 0-65.7-8.9-94-25.7l-6.7-4-69.8 18.3L72 359.2l-4.4-7c-18.5-29.4-28.2-63.3-28.2-98.2 0-101.7 82.8-184.5 184.6-184.5 49.3 0 95.6 19.2 130.4 54.1 34.8 34.9 56.2 81.2 56.1 130.5 0 101.8-84.9 184.6-186.6 184.6zm101.2-138.2c-5.5-2.8-32.8-16.2-37.9-18-5.1-1.9-8.8-2.8-12.5 2.8-3.7 5.6-14.3 18-17.6 21.8-3.2 3.7-6.5 4.2-12 1.4-32.6-16.3-54-29.1-75.5-66-5.7-9.8 5.7-9.1 16.3-30.3 1.8-3.7.9-6.9-.5-9.7-1.4-2.8-12.5-30.1-17.1-41.2-4.5-10.8-9.1-9.3-12.5-9.5-3.2-.2-6.9-.2-10.6-.2-3.7 0-9.7 1.4-14.8 6.9-5.1 5.6-19.4 19-19.4 46.3 0 27.3 19.9 53.7 22.6 57.4 2.8 3.7 39.1 59.7 94.8 83.8 35.2 15.2 49 16.5 66.6 13.9 10.7-1.6 32.8-13.4 37.4-26.4 4.6-13 4.6-24.1 3.2-26.4-1.3-2.5-5-3.9-10.5-6.6z"/></svg>
                     0982530072
                </a>
            </div>
        </div>`,
        });
    });
}

async function getVerifyImei(imei: string) {
    try {
        const _data = await fetch("/verify-imei/" + imei);
        const data = await _data.json();
        return data;
        return Promise.resolve(data);
    } catch (error) {
        alert("fallida");
        return Promise.reject(error);
    }
}

async function getHeists() {
    try {
        const _data = await fetch("/heists");
        const data = await _data.json();
        return data;
        return Promise.resolve(data);
    } catch (error) {
        alert("fallida");
        return Promise.reject(error);
    }
}

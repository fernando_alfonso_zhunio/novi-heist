import { DialogCustom } from "./DialogCustom";

export function Alert({children, title, msg, isOpen }: { children: JSX.Element, title: string, msg: string, isOpen: boolean }) {
    return (
        <DialogCustom isOpen={isOpen}>
            <div>
                <h2 className="text-lg font-medium text-gray-900">{title}</h2>
                <p>{msg}</p>
                {children}
            </div>
        </DialogCustom>
        
    )
}
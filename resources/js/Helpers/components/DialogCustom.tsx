import { createPortal } from "react-dom";

export function DialogCustom({
    children,
    maxWidth,
    maxHeight,
    isOpen,
}: any) {
    return (
        isOpen ?
        createPortal(
            <div className="fixed backdrop-blur-sm center top-0 left-0 bg-white/10 z-10 h-full w-full overflow-hidden ">
                <div style={{ maxWidth, maxHeight }}  className="bg-white p-5 shadow-lg rounded-xl">
                    {children}
                </div>
            </div>,
            document.body
        ) : null
    );
}

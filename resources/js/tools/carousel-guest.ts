export class Carousel {
    private carousel!: HTMLElement;
    private overlay!: HTMLElement;
    private isDragging = false;
    private initScroll = 0;
    private scrollLeft = 0;
    private previousScrollLeft = 0;
    private currentIndex = 0;
    private currentScroll = 0;

    private velX = 0;
    private momentumID: any;
    constructor(private querySelector: string) {
        this.carousel = document.querySelector(this.querySelector)!;
        // this.overlay = document.createElement('div');
        // this.overlay.classList.add('overlay');
        // this.carousel.appendChild(this.overlay);
    }

    init() {
        this.carousel.addEventListener("mousedown", this.start.bind(this));
        this.carousel.addEventListener("touchstart", this.start.bind(this));

        this.carousel.addEventListener("mousemove", this.move.bind(this));
        this.carousel.addEventListener("touchmove", this.move.bind(this));

        this.carousel.addEventListener("mouseup", this.end.bind(this));

        this.carousel.addEventListener("mouseleave", this.end.bind(this));
        this.carousel.addEventListener("mouseup", this.end.bind(this));
        this.carousel.addEventListener("touchend", this.end.bind(this));

        const btnNext = document.querySelector('#btn-carousel-next')!;
        const btnPrev = document.querySelector('#btn-carousel-prev')!;

        btnNext.addEventListener('click', this.next.bind(this));
        btnPrev.addEventListener('click', this.previous.bind(this));
    }

    next() {  
        const next = this.carousel.scrollLeft + this.carousel.clientWidth;
        this.carousel.scrollTo({left: this.carousel.scrollWidth < next ? 0 : next, behavior: 'smooth'});
    }
    previous() {
        const prev = this.carousel.scrollLeft - this.carousel.clientWidth;
        this.carousel.scrollTo({left: prev < 0 ? 0 : prev, behavior: 'smooth'});
    }

    start(e: any) {
        console.log("mouse down");
        this.isDragging = true;
        this.initScroll = e.pageX - this.carousel.offsetLeft;
        this.scrollLeft = this.carousel.scrollLeft;
        this.carousel.classList.remove("snap-carousel");
        this.cancelMomentumTracking();
    }

    move(e: any) {
        if (!this.isDragging) {
            return;
        }
        e.preventDefault();
        const currentPosition = e.clientX;
        const x = e.pageX - this.carousel.offsetLeft;
        const dist = x - this.initScroll;
        this.previousScrollLeft = this.carousel.scrollLeft;
        this.carousel.scrollLeft = this.scrollLeft - dist;
        this.velX = this.carousel.scrollLeft - this.previousScrollLeft;
    }

    end(e: any) {
        this.isDragging = false;
        this.beginMomentumTracking();
        this.carousel.classList.add("snap-carousel");
    }

    beginMomentumTracking() {
        this.cancelMomentumTracking();
        this.momentumID = requestAnimationFrame(this.momentumLoop.bind(this));
    }

    cancelMomentumTracking() {
        cancelAnimationFrame(this.momentumID);
    }

    momentumLoop(){
        this.carousel.scrollLeft += this.velX;
        this.velX *= 0.95; 
        if (Math.abs(this.velX) > 0.5){
          this.momentumID = requestAnimationFrame(this.momentumLoop.bind(this));
        }
      }
}

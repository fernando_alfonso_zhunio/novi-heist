export class AlertFz {
    parent!: HTMLElement;
    constructor(private querySelector: string) {
        this.parent = document.querySelector(this.querySelector)!;
        this.parent.querySelector('button')?.addEventListener('click', () => {
            this.hide();
        })
    }

    show({message, title}: {message: string, title: string}) {
        this.parent.classList.remove('hidden');
        this.parent.classList.add('flex');

        this.parent.querySelector('h3')!.textContent = title;
        this.parent.querySelector('p')!.innerHTML = message;
        this.parent.querySelector('button')!.focus();
    }



    hide() {
        this.parent.classList.remove('flex');
        this.parent.classList.add('hidden');
    }
}
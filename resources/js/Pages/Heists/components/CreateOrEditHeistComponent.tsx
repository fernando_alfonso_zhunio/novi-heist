import { router, useForm } from "@inertiajs/react";
import { useEffect, useState } from "react";
import { IHeist } from "../types/heist";

export default function CreateOrEditHeistComponent({
    onClose,
    heist,
}: {
    onClose: CallableFunction;
    heist: IHeist | null;
}) {
    const { data, setData, post, put, processing, errors } = useForm({
        name: "",
        file: null,
        reward: 0,
        city: "",
        description: "",
        title: "",
    });

    const [url, setUrl] = useState<string | null>(null);
    function submit(e: any) {
        e.preventDefault();
        if (heist) {
            router.post(`/admin/heist/${heist.id}`, 
                 {...data, _method: 'PUT'},
                {
                    forceFormData: true,
                    onSuccess: () => onClose(),

                }
            );
            return;
        }
        post("/admin/heist", {
            forceFormData: true,
            onSuccess: () => onClose(),
        });
    }

    useEffect(() => {
        if (heist) {
            setData({
                city: heist.city,
                description: heist.description,
                name: heist.name,
                reward: +heist.reward,
                title: heist.title,
                file: null,
            });
            setUrl(heist.path);
            console.log(heist);
        }
    }, []);

    function convertFileToBase64(file: any) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = (error) => reject(error);
        });
    }

    async function onHandlerFile(e: any) {
        // setFileBase64(await convertFileToBase64(e.target.files[0]));
        setData("file", e.target.files[0]);
    }
    return (
        <div>
            {errors && (
                <div className="text-red-500 my-2">
                    {Object.values(errors).map((error) => (
                        <p className="block" key={error}>
                            {error}
                        </p>
                    ))}
                </div>
            )}
            <form className="flex flex-col gap-2" onSubmit={submit}>
                <div>
                    {/* <label htmlFor="file">Imagen</label>
                    <input type="file"  onChange={(e: any) => setData('file', e.target.files[0])} /> */}
                    <label className="block">
                        <span className="sr-only">Choose profile photo</span>
                        <input
                            type="file"
                            onChange={onHandlerFile}
                            className="block w-full text-sm text-slate-500
      file:mr-4 file:py-2 file:px-4
      file:rounded-full file:border-0
      file:text-sm file:font-semibold
      file:bg-violet-50 file:text-violet-700
      hover:file:bg-violet-100 outline-0
    "
                        />
                    </label>
                </div>
                <div>
                    <label htmlFor="title">Titulo</label>
                    <input
                        type="text"
                        id="title"
                        placeholder="Titulo"
                        value={data.title}
                        onChange={(e) => setData("title", e.target.value)}
                        className="bg-slate-100 block w-full border-none rounded-md"
                    />
                </div>
                <div>
                    <label htmlFor="name">Nombre</label>
                    <input
                        type="text"
                        id="name"
                        placeholder="Nombre"
                        value={data.name}
                        onChange={(e) => setData("name", e.target.value)}
                        className="mt-1 block w-full bg-slate-100 border-none rounded-md"
                    />
                </div>
                <div>
                    <label htmlFor="reward">Recompensa</label>
                    <input
                        type="number"
                        id="reward"
                        placeholder="Recompensa"
                        value={data.reward}
                        onChange={(e) =>
                            setData("reward", Number(e.target.value))
                        }
                        className="mt-1 block bg-slate-100 w-full border-none rounded-md"
                    />
                </div>
                <div>
                    <label htmlFor="reward">Ciudad</label>
                    <input
                        type="text"
                        id="city"
                        placeholder="Ciudad"
                        value={data.city}
                        onChange={(e) => setData("city", e.target.value)}
                        className="mt-1 block bg-slate-100 w-full border-none rounded-md"
                    />
                </div>
                <div>
                    <label htmlFor="description">Descripción</label>
                    <textarea
                        id="description"
                        placeholder="Nombre"
                        value={data.description}
                        onChange={(e) => setData("description", e.target.value)}
                        className="mt-1 block bg-slate-100 w-full border-none rounded-md"
                    ></textarea>
                </div>
                <div className="mt-4 flex gap-2">
                    <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                        Guardar
                    </button>
                    <button
                        onClick={() => onClose(false)}
                        className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
                    >
                        Cerrar
                    </button>
                </div>
            </form>
        </div>
    );
}

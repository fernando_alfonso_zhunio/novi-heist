import { IPaginator } from "@/Helpers/types/paginator";
import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { PageProps } from "@/types";
import { Head, router } from "@inertiajs/react";
import { IHeist } from "./types/heist";
import CreateOrEditHeistComponent from "./components/CreateOrEditHeistComponent";
import { DialogCustom } from "@/Helpers/components/DialogCustom";
import { useState } from "react";
import { Alert } from "@/Helpers/components/Alert";

export default function HeistIndex({
    auth,
    heists,
}: PageProps<{ heists: IPaginator<IHeist> }>) {
    console.log(heists);
    function nextPage() {}
    const [heist, setHeist] = useState<IHeist | null>(null);

    function edit(id: number) {
        setHeist(heists.data.find((heist) => heist.id === id)!);
        setIsOpen(true);
    }

    function create() {
        setHeist(null);
        setIsOpen(true);
    }

    const [isOpen, setIsOpen] = useState(false);
    const [isOpenAlert, setIsOpenAlert] = useState(false);
    const [currentHeist, setCurrentHeist] = useState<IHeist | null>(null);

    function openDeleteAlert(heist: IHeist) {
        setCurrentHeist(heist);
        setIsOpenAlert(true);
    }

    function deleteHeist() {
        setCurrentHeist(null);
        setIsOpenAlert(false);
        router.delete('/admin/heist/'+ currentHeist!.id);
    }

    return (
        <AuthenticatedLayout
            user={auth.user}
            header={
                <div className="flex justify-between items-center">
                    <h2 className="font-semibold text-xl text-gray-800 leading-tight">
                        Publicaciones de robos
                    </h2>
                    <button
                        onClick={create}
                        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                    >
                        Agregar
                    </button>
                </div>
            }
        >
            <Head title="robos" />

            <div className="py-12 px-7">
                <div className="w-full bg-white rounded-lg p-4">
                    <table className="table-auto w-full">
                        <thead>
                            <tr>
                                <th>Imagen</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Recompensa</th>
                                <th>Ciudad</th>
                                <th>Titulo</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {heists.data.map((heist) => (
                                <tr className="border-b" key={heist.id}>
                                    <td className="text-center px-2 py-1">
                                        <img
                                            className="inline-block h-12 w-12 rounded-full ring-2 ring-white"
                                            src={heist.path}
                                            alt=""
                                        />
                                    </td>
                                    <td className="text-center px-2 py-1">
                                        {heist.name}
                                    </td>
                                    <td className="text-center px-2 py-1">
                                        {heist.description}
                                    </td>
                                    <td className="text-center px-2 py-1">
                                        {heist.reward}
                                    </td>
                                    <td className="text-center px-2 py-1">
                                        {heist.city}
                                    </td>
                                    <td className="text-center px-2 py-1">
                                        {heist.title}
                                    </td>
                                    <td className="text-center px-2 py-1 ">
                                        <div className="flex gap-3 items-center justify-center">
                                            <button
                                                onClick={() => edit(heist.id)}
                                            >
                                                <svg
                                                    fill="blue"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    height="1em"
                                                    viewBox="0 0 512 512"
                                                >
                                                    <path d="M441 58.9L453.1 71c9.4 9.4 9.4 24.6 0 33.9L424 134.1 377.9 88 407 58.9c9.4-9.4 24.6-9.4 33.9 0zM209.8 256.2L344 121.9 390.1 168 255.8 302.2c-2.9 2.9-6.5 5-10.4 6.1l-58.5 16.7 16.7-58.5c1.1-3.9 3.2-7.5 6.1-10.4zM373.1 25L175.8 222.2c-8.7 8.7-15 19.4-18.3 31.1l-28.6 100c-2.4 8.4-.1 17.4 6.1 23.6s15.2 8.5 23.6 6.1l100-28.6c11.8-3.4 22.5-9.7 31.1-18.3L487 138.9c28.1-28.1 28.1-73.7 0-101.8L474.9 25C446.8-3.1 401.2-3.1 373.1 25zM88 64C39.4 64 0 103.4 0 152V424c0 48.6 39.4 88 88 88H360c48.6 0 88-39.4 88-88V312c0-13.3-10.7-24-24-24s-24 10.7-24 24V424c0 22.1-17.9 40-40 40H88c-22.1 0-40-17.9-40-40V152c0-22.1 17.9-40 40-40H200c13.3 0 24-10.7 24-24s-10.7-24-24-24H88z" />
                                                </svg>
                                            </button>
                                            <div className="relative">
                                                <button onClick={() => openDeleteAlert(heist)}>
                                                    <svg
                                                        fill="red"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        height="1em"
                                                        viewBox="0 0 448 512"
                                                    >
                                                        <path d="M135.2 17.7C140.6 6.8 151.7 0 163.8 0H284.2c12.1 0 23.2 6.8 28.6 17.7L320 32h96c17.7 0 32 14.3 32 32s-14.3 32-32 32H32C14.3 96 0 81.7 0 64S14.3 32 32 32h96l7.2-14.3zM32 128H416V448c0 35.3-28.7 64-64 64H96c-35.3 0-64-28.7-64-64V128zm96 64c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16zm96 0c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16zm96 0c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16z" />
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            ))}
                            {heists.data.length < 1 && (
                                <tr className="bg-gray-100">
                                    <td className="p-3" colSpan={7}>
                                        No hay publicaciones
                                    </td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>

                <DialogCustom isOpen={isOpen}>
                    <CreateOrEditHeistComponent
                        heist={heist}
                        onClose={setIsOpen}
                    ></CreateOrEditHeistComponent>
                </DialogCustom>

                <Alert
                    title="Precaución"
                    isOpen={isOpenAlert}
                    msg="Esta seguro de eliminar este robo"
                >
                    <div className="flex gap-3 mt-3">
                        <button className="bg-blue-700 w-20  text-white rounded-md px-3 py-2" onClick={() => deleteHeist()}>Si</button>
                        <button className="bg-red-700 w-20 text-white rounded-md px-3 py-2" onClick={() => setIsOpenAlert(false)}>No</button>
                    </div>
                </Alert>
            </div>
        </AuthenticatedLayout>
    );
}

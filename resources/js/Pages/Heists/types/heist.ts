export interface IHeist {
    id:number;
    path:string;
    name: string;
    title: string;
    type: string;
    description: string;
    reward: string;
    city: string;
}
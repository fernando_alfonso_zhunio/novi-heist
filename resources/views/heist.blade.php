@extends('app-guest')

@section('content')
    <header>
        <x-navbar></x-navbar>
    </header>
    <section id="home" class="h-screen bg-slate-100 w-full flex items-center md:flex-row flex-col justify-center md:px-10 px-4 gap-3">
        <div class="md:w-1/2 ">
            <h2 class="md:text-6xl text-3xl">Lucha Contra el Robo: Ayuda a Identificar a los Culpables</h2>
            <h3 class="text-4xl text-blue-600">Ademas Verifica si tu Dispositivo es Robado</h3>
            <p class="text-gray-700 mt-3">La seguridad de tus dispositivos es una prioridad. Con nuestra herramienta, puedes verificar la autenticidad de tu dispositivo y protegerte contra la adquisición de dispositivos robados.</p>
        </div>
        <div class="md:w-1/2 flex justify-center justify-content-center items-center">
            <form class="inline-block bg-white p-4 rounded-lg md:w-1/2 sm:w-2/3 shadow-sm">
                <div class="text-2xl mb-3">Formulario de Verificación de Dispositivos Robados</div>
                {{-- <div class="mb-3">
                    <label for="code_imei">Nombres</label>
                    <input class="inline-block w-full rounded-md border-none bg-gray-200" type="text" name="code_imei" id="code_imei" placeholder="Ingrese tu nombre">
                </div> --}}
                <div>
                    <label for="code_imei">Codigo IMEI*</label>
                    <input id="input-imei" class="inline-block w-full rounded-md border-none bg-gray-200" type="text" name="code_imei" id="code_imei" placeholder="Ingrese el codigo imei">
                </div>
                <div class="mt-4 text-center w-full">
                    <button id="btn-verify" class="bg-blue-500 w-full hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Comprobar</button>
                </div>
            </form>
        </div>
    </section>

    <section id="photo-and-video" class="my-16">
        <h3  class="text-6xl mt-4 text-center mb-7 font-bold">Evidencia de robos</h3>
        <x-carousel :heists="$heists"></x-carousel>
    </section>
    {{-- <section id="questions" class="my-32">
        <h3 class="text-6xl mt-4 text-center mb-7 font-bold">Preguntas</h3>
        <x-questions></x-questions>
    </section> --}}
    <section id="contact" class="my-32">
        <h3 class="text-6xl mt-4 text-center mb-7 font-bold">Contactos</h3>
        <x-contact></x-contact>
    </section>
    <x-alert></x-alert>
@endsection
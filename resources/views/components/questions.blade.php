<div class="md:px-10 px-3 flex flex-col gap-5">
    <div class="text-center">
        <h4 class="text-2xl p-3 shadow rounded-lg inline-block">¿Cómo puedo proporcionar información de manera anónima?</h4>
        <p class="text-white bg-slate-800 w-2/3 p-3 mt-4 shadow rounded-lg inline-block">Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim neque autem animi, soluta non omnis deleniti at quia natus. Ab nemo optio labore molestiae nostrum veniam qui voluptatibus reiciendis dicta!</p>
    </div>

    <div class="text-center">
        <h4 class="text-2xl p-3 shadow rounded-lg inline-block">¿Cuál es el proceso para reclamar una recompensa por la captura de un culpable?</h4>
        <p  class="text-white bg-slate-800 w-2/3 p-3 mt-4 shadow rounded-lg inline-block">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, possimus, ea incidunt fugit minima deleniti voluptates, nihil tempore obcaecati ut debitis blanditiis aspernatur accusantium illo nulla in dicta adipisci nesciunt!</p>
    </div>

   <div class="text-center">
     <h4  class="text-2xl p-3 shadow rounded-lg inline-block">¿Qué debo hacer si creo que mi teléfono ha sido robado?</h4>
     <p  class="text-white bg-slate-800 w-2/3 p-3 mt-4 shadow rounded-lg inline-block">Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis error dolores quia qui harum. At minus harum, eum temporibus qui necessitatibus vel quod eos dolorem neque, modi nihil sint architecto?</p>
   </div>
</div>
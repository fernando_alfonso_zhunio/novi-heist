<div id="alert-custom" class="hidden fixed w-full h-full z-40 p-4 top-0 left-0 justify-center items-center backdrop-blur-sm bg-white/5">
    <div class="p-7 rounded-lg bg-white shadow-lg md:w-1/3  ">
        <h3 class="alert-title mb-3 font-bold text-3xl text-center"></h3>
        <p id="alert-message" class="text-center"></p>
        <divv id="alert-btn" class="flex justify-center items-center mt-3">
            <button class="bg-green-700 hover:bg-green-500 text-white font-bold py-2 px-4 rounded">Ok</button>
        </divv>
    </div>
</div>
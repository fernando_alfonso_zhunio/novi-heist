<div class="md:px-16 px-10 mt-12">
    {{-- <div class="relative flex">
        <button id="btn-carousel-prev" style="left: -20px" class="left-0 z-10 absolute top-1/2 bg-blue-400 w-12 center aspect-square rounded-full shadow-md"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 -960 960 960" width="24"><path d="M560-240 320-480l240-240 56 56-184 184 184 184-56 56Z"/></svg></button>
        <div id="carousel" class="can-move-carousel relative flex justify-start gap-5 overflow-x-auto overflow-y-hidden p-3">
            @foreach ([1, 2, 3, 4, 5, 5, 5, 5, 5] as $item)
            <div style="min-width: 270px" class="item-carousel bg-white rounded-xl overflow-hidden shadow">
                <div>
                    <img src="https://equipamientoyseguridad.com/wp-content/uploads/diferencias-robo-hurto-atraco-768x462.jpg" alt="">
                </div>
                <div class="p-5">
                    <div>
                        <h4 class="font-bold">Ladron dos camaras</h4>
                        <p>Ubicacion: Guayaquil</p>
                        <small>Recompenza: 1000$</small>
                    </div>
                    <a class="text-blue-600 font-light text-sm" href="http://" target="_blank" rel="noopener noreferrer">Tengo informacion</a>
                </div>
            </div>
            @endforeach
        </div>
        <button id="btn-carousel-next" style="right: -20px" class="right-0 z-10 absolute top-1/2 bg-blue-400 w-12 center aspect-square rounded-full shadow-md"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 -960 960 960" width="24"><path d="M504-480 320-664l56-56 240 240-240 240-56-56 184-184Z"/></svg></button>
    </div> --}}
    <div id="content-heists" class="grid grid-cols-1 md:grid-cols-3 gap-4">
        {{-- @foreach ($heists as $key => $heist)
        <div class="bg-white rounded-lg">
            @if ($heist->type == 'img')
            <img loading="lazy" class="h-auto aspect-square object-contain bg-black max-w-full rounded-lg" src="{{$heist->path}}" alt="">
            @else
            <iframe loading="lazy" width="100%" height="auto" controls class="h-auto aspect-square object-contain bg-black max-w-full rounded-lg" src="https://www.youtube.com/embed/{{$heist->path}}" alt=""></iframe>   
            @endif
            <div class="px-7 py-3">
                <h4 class="font-bold mb-3">{{$heist->name}}</h4>
                <a class="text-white font-light text-sm bg-blue-600 p-3 mb-3 rounded-md" href="http://" target="_blank" rel="noopener noreferrer">Tengo informacion</a>
            </div>
        </div>
        @endforeach --}}
        {{-- <div>
            <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-1.jpg" alt="">
        </div>
        <div>
            <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-2.jpg" alt="">
        </div>
        <div>
            <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-3.jpg" alt="">
        </div>
        <div>
            <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-4.jpg" alt="">
        </div>
        <div>
            <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-5.jpg" alt="">
        </div>
        <div>
            <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-6.jpg" alt="">
        </div>
        <div>
            <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-7.jpg" alt="">
        </div>
        <div>
            <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-8.jpg" alt="">
        </div>
        <div>
            <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-9.jpg" alt="">
        </div>
        <div>
            <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-10.jpg" alt="">
        </div>
        <div>
            <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-11.jpg" alt="">
        </div> --}}
        {{-- add skeleton loader  --}}

        {{-- <div role="status" class="max-w-sm p-4  border-gray-200 rounded animate-pulse  dark:border-gray-700">
            <div class="flex items-center justify-center h-48 mb-4 bg-gray-300 rounded dark:bg-gray-400">
                <svg class="w-10 h-10 text-gray-200 dark:text-gray-400" aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 16 20">
                    <path
                        d="M14.066 0H7v5a2 2 0 0 1-2 2H0v11a1.97 1.97 0 0 0 1.934 2h12.132A1.97 1.97 0 0 0 16 18V2a1.97 1.97 0 0 0-1.934-2ZM10.5 6a1.5 1.5 0 1 1 0 2.999A1.5 1.5 0 0 1 10.5 6Zm2.221 10.515a1 1 0 0 1-.858.485h-8a1 1 0 0 1-.9-1.43L5.6 10.039a.978.978 0 0 1 .936-.57 1 1 0 0 1 .9.632l1.181 2.981.541-1a.945.945 0 0 1 .883-.522 1 1 0 0 1 .879.529l1.832 3.438a1 1 0 0 1-.031.988Z" />
                    <path d="M5 5V.13a2.96 2.96 0 0 0-1.293.749L.879 3.707A2.98 2.98 0 0 0 .13 5H5Z" />
                </svg>
            </div>
            <div class="h-10 bg-gray-200 rounded-full dark:bg-gray-400  mb-4"></div>
            <div class="h-10 bg-gray-200 rounded-full dark:bg-gray-400"></div>
            <span class="sr-only">Loading...</span>
        </div> --}}
    </div>
    <div id="skeleton-heists" class="grid grid-cols-2 md:grid-cols-3 gap-4 mt-5">
        <div role="status" class=" border-gray-200 rounded animate-pulse  dark:border-gray-700">
            <div class="flex items-center justify-center h-48 mb-4 bg-gray-300 rounded dark:bg-gray-400">
                <svg class="w-10 h-10 text-gray-200 dark:text-gray-400" aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 16 20">
                    <path
                        d="M14.066 0H7v5a2 2 0 0 1-2 2H0v11a1.97 1.97 0 0 0 1.934 2h12.132A1.97 1.97 0 0 0 16 18V2a1.97 1.97 0 0 0-1.934-2ZM10.5 6a1.5 1.5 0 1 1 0 2.999A1.5 1.5 0 0 1 10.5 6Zm2.221 10.515a1 1 0 0 1-.858.485h-8a1 1 0 0 1-.9-1.43L5.6 10.039a.978.978 0 0 1 .936-.57 1 1 0 0 1 .9.632l1.181 2.981.541-1a.945.945 0 0 1 .883-.522 1 1 0 0 1 .879.529l1.832 3.438a1 1 0 0 1-.031.988Z" />
                    <path d="M5 5V.13a2.96 2.96 0 0 0-1.293.749L.879 3.707A2.98 2.98 0 0 0 .13 5H5Z" />
                </svg>
            </div>
            <div class="h-10 bg-gray-200 rounded-full dark:bg-gray-400  mb-4"></div>
            <div class="h-10 bg-gray-200 rounded-full dark:bg-gray-400"></div>
            <span class="sr-only">Loading...</span>
        </div>

        <div role="status" class="  border-gray-200 rounded animate-pulse  dark:border-gray-700">
            <div class="flex items-center justify-center h-48 mb-4 bg-gray-300 rounded dark:bg-gray-400">
                <svg class="w-10 h-10 text-gray-200 dark:text-gray-400" aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 16 20">
                    <path
                        d="M14.066 0H7v5a2 2 0 0 1-2 2H0v11a1.97 1.97 0 0 0 1.934 2h12.132A1.97 1.97 0 0 0 16 18V2a1.97 1.97 0 0 0-1.934-2ZM10.5 6a1.5 1.5 0 1 1 0 2.999A1.5 1.5 0 0 1 10.5 6Zm2.221 10.515a1 1 0 0 1-.858.485h-8a1 1 0 0 1-.9-1.43L5.6 10.039a.978.978 0 0 1 .936-.57 1 1 0 0 1 .9.632l1.181 2.981.541-1a.945.945 0 0 1 .883-.522 1 1 0 0 1 .879.529l1.832 3.438a1 1 0 0 1-.031.988Z" />
                    <path d="M5 5V.13a2.96 2.96 0 0 0-1.293.749L.879 3.707A2.98 2.98 0 0 0 .13 5H5Z" />
                </svg>
            </div>
            <div class="h-10 bg-gray-200 rounded-full dark:bg-gray-400  mb-4"></div>
            <div class="h-10 bg-gray-200 rounded-full dark:bg-gray-400"></div>
            <span class="sr-only">Loading...</span>
        </div>

        <div role="status" class="  border-gray-200 rounded animate-pulse  dark:border-gray-700">
            <div class="flex items-center justify-center h-48 mb-4 bg-gray-300 rounded dark:bg-gray-400">
                <svg class="w-10 h-10 text-gray-200 dark:text-gray-400" aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 16 20">
                    <path
                        d="M14.066 0H7v5a2 2 0 0 1-2 2H0v11a1.97 1.97 0 0 0 1.934 2h12.132A1.97 1.97 0 0 0 16 18V2a1.97 1.97 0 0 0-1.934-2ZM10.5 6a1.5 1.5 0 1 1 0 2.999A1.5 1.5 0 0 1 10.5 6Zm2.221 10.515a1 1 0 0 1-.858.485h-8a1 1 0 0 1-.9-1.43L5.6 10.039a.978.978 0 0 1 .936-.57 1 1 0 0 1 .9.632l1.181 2.981.541-1a.945.945 0 0 1 .883-.522 1 1 0 0 1 .879.529l1.832 3.438a1 1 0 0 1-.031.988Z" />
                    <path d="M5 5V.13a2.96 2.96 0 0 0-1.293.749L.879 3.707A2.98 2.98 0 0 0 .13 5H5Z" />
                </svg>
            </div>
            <div class="h-10 bg-gray-200 rounded-full dark:bg-gray-400  mb-4"></div>
            <div class="h-10 bg-gray-200 rounded-full dark:bg-gray-400"></div>
            <span class="sr-only">Loading...</span>
        </div>

    </div>

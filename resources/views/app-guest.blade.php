<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title inertia>Denuncias Novicompu</title>
        <link rel="shortcut icon" href="assets/images/favicon.webp" type="image/x-icon">
        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

        <!-- Scripts -->
        @viteReactRefresh
        @vite(['resources/js/app-guest.tsx'])
    </head>
    <body class="font-sans antialiased bg-slate-100">
        {{-- @include('app-guest') --}}
        @yield('content')
    </body>
</html>
